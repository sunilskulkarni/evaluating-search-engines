import math
import sys
import time
import metapy
import pytoml

class MyOkapiBM25(metapy.index.RankingFunction):
    """
    Create a new ranking function in Python that can be used in MeTA.
    """
    def __init__(self, k1_=1.2, b_=0.75, k3_ = 500):
        self.k1 = k1_
        self.b = b_
        self.k3 = k3_
        # You *must* call the base class constructor here!
        super(MyOkapiBM25, self).__init__() #try BM25 otherwise

    def score_one(self, sd):
        doc_len = sd.idx.doc_size(sd.d_id)
        IDF = math.log(1.0 + (sd.num_docs - sd.doc_count + 0.5) / (sd.doc_count + 0.5))
        TF = ((self.k1 + 1.0) * sd.doc_term_count) / ((self.k1 * ((1.0 - self.b) + self.b * doc_len / sd.avg_dl)) + sd.doc_term_count)
        QTF = ((self.k3 + 1.0) * sd.query_term_weight) / (self.k3 + sd.query_term_weight)
        return TF * IDF * QTF

class PL2Ranker(metapy.index.RankingFunction):
    """
    Create a new ranking function in Python that can be used in MeTA.
    """
    def __init__(self, c_param=1.0):
        self.c = c_param
        # You *must* call the base class constructor here!
        super(PL2Ranker, self).__init__() #try BM25 otherwise

    def score_one(self, sd):
        """
        You need to override this function to return a score for a single term.
        For fields available in the score_data sd object,
        @see https://meta-toolkit.org/doxygen/structmeta_1_1index_1_1score__data.html
        """
        lamda = sd.num_docs / sd.corpus_term_count
        tfn = sd.doc_term_count * math.log2(1.0 + self.c * sd.avg_dl / sd.doc_size)
        if lamda < 1 or tfn <= 0:
            return 0.0
        numerator = tfn * math.log2(tfn * lamda) \
                        + math.log2(math.e) * (1.0 / lamda - tfn) \
                        + 0.5 * math.log2(2.0 * math.pi * tfn)
        return sd.query_term_weight * numerator / (tfn + 1.0)


class mptf2ln(metapy.index.RankingFunction):
    """
    Create a new ranking function in Python that can be used in MeTA.
    """
    def __init__(self, s=0.2,mu=2000.0,alpha_=0.3,lambda_=0.7):
        self.s = s
        self.mu = mu
        self.alpha_ = alpha_
        self.lambda_ = lambda_
        # You *must* call the base class constructor here!
        super(mptf2ln, self).__init__() #try BM25 otherwise

    def score_one(self, sd):
        doc_len = sd.idx.doc_size(sd.d_id)
        avg_dl = sd.avg_dl
        tf = sd.doc_term_count
        df = sd.doc_count
        pc = (sd.corpus_term_count) / sd.total_terms

        tfok = 2.2 * tf / (1.2 + tf)
        idfpiv = math.log((sd.num_docs + 1.0) / df)
        tfidfdir = math.log(1.0 + tf / (self.mu * pc))
        lnpiv = 1 - self.s + self.s * doc_len / avg_dl
        tfidf2 = self.alpha_ * tfok * idfpiv + (1.0 - self.alpha_) * tfidfdir
        score = sd.query_term_weight * tfidf2 / pow(lnpiv, self.lambda_ )
        return score

def load_ranker(cfg_file):
    """
    Use this function to return the Ranker object to evaluate, 
    The parameter to this function, cfg_file, is the path to a
    configuration file used to load the index.
    """
    #baseline	0.6166	-inf	0.3495	-inf	0.7868	-inf	0.3966	-inf	2019-10-14 | 20:25:06	1
    #return metapy.index.OkapiBM25(k1=2.3, b=0.65, k3=500)
    #sunilk2	0.585	-inf	0.3609	-inf	0.7279	-inf	0.3997	-inf	2019-10-15 | 14:42:10	1

    #return MyOkapiBM25(k1_=2.3, b_=0.65, k3_=500)
    #sunilk2	0.585	0.4658	0.361	0.3506	0.7279	0.5366	0.3997	0.386

    #return MyOkapiBM25(k1_=2.3, b_=0.75, k3_=800)
    #sunilk2	0.6078	0.2617	0.3601	0.3091	0.7662	0.2134	0.4007	0.4093

    #return MyOkapiBM25(k1_=2.3, b_=0.75, k3_=100)
    #sunilk2	0.6079	0.6038	0.3602	0.3567	0.7662	0.7621	0.4007	0.3948

    #return MyOkapiBM25(k1_=2.3, b_=0.7, k3_=100)
    #sunilk2	0.6035	0.6079	0.36	0.3602	0.7596	0.7662	0.3979	0.4007

    #return MyOkapiBM25(k1_=2.3, b_=0.6, k3_=200)
    #sunilk2	0.5738	0.5738	0.3559	0.3559	0.7106	0.7106	0.4067	0.4067

    #return MyOkapiBM25(k1_=2.3, b_=0.8, k3_=800)
    #sunilk2	0.6038	0.6078	0.3567	0.3601	0.7621	0.7662	0.3948	0.4007

    #return MyOkapiBM25(k1_=0.5, b_=0, k3_=100) : WORST!
    #sunilk2	0.2617	0.585	0.3091	0.361	0.2134	0.7279	0.4093	0.3997

    #return metapy.index.OkapiBM25(k1=1.7, b=0.5, k3=800)
    #sunilk2	0.5565	0.585	0.352	0.3609	0.6832	0.7279	0.4102	0.3997


    #return MyOkapiBM25(k1_=2.2, b_=0.75, k3_=100)
    #sunilk2    0.6091	0.5738	0.3598	0.3559	0.7684	0.7106	0.4015	0.4067

    #return MyOkapiBM25(k1_=2.2, b_=0.75, k3_=800)
    #sunilk2	0.6093	0.6091	0.3602	0.3598	0.7684	0.7684	0.4015	0.4015

    #return MyOkapiBM25(k1_=2.1, b_=0.75, k3_=800)
    #sunilk2	0.611	0.6093	0.3593	0.3602	0.7718	0.7684	0.4016	0.4015

    #return metapy.index.OkapiBM25(k1=2.1, b=0.75, k3=800)
    # sunilk2	0.611	0.611	0.3593	0.3593	0.7718	0.7718	0.4016	0.4016

    #return metapy.index.OkapiBM25(k1=2.0, b=0.75, k3=800)
    #sunilk2	0.6112	0.611	0.36	0.3593	0.7719	0.7718	0.4005	0.4016

    #return metapy.index.OkapiBM25(k1=2.0, b=0.9, k3=800)
    #sunilk2	0.5627	0.6112	0.3564	0.36	0.6954	0.7719	0.3856	0.4005	2019-10-15 | 18:13:37

    #return metapy.index.OkapiBM25(k1=2.0, b=0.5, k3=800)
    #sunilk2	0.5496	0.5627	0.3541	0.3564	0.671	0.6954	0.4078	0.3856	2

    #return metapy.index.OkapiBM25(k1=2.0, b=0.75, k3=500)
    #sunilk2	0.6112	0.5496	0.36	0.3541	0.7719	0.671	0.4005	0.4078

    #return metapy.index.OkapiBM25(k1=1.5, b=0.75, k3=500)
    #sunilk2	0.6163	0.6112	0.3543	0.36	0.7834	0.7719	0.4002	0.4005

    #return metapy.index.OkapiBM25(k1=1.0, b=0.75, k3=500)
    #sunilk2	0.62	0.6163	0.3483	0.3543	0.7924	0.7834	0.4009	0.4002
    #sunilk2	0.1503	0.1504	0.0095	0.0095	0.3483	0.348	0.4009	0.4031

    #return metapy.index.OkapiBM25(k1=0.8, b=0.75, k3=500)
    #sunilk2	0.6195	0.6177	0.3448	0.3342	0.7931	0.7961	0.4024	0.397

    #return metapy.index.OkapiBM25(k1=0.9, b=0.75, k3=500)
    #sunilk2	0.6203	0.6195	0.348	0.3448	0.7926	0.7931	0.4031	0.4024

    #return metapy.index.OkapiBM25(k1=0.9, b=0.75, k3=800)
    #sunilk2	0.6203	0.6203	0.348	0.348	0.7926	0.7926	0.4031	0.4031
    #- GOOD#15 leaderboard, now 71

    # k1=1.2; b=0.75; k3=7.0
    #return metapy.index.OkapiBM25(k1=0.9, b=0.75, k3=7.0)
    #sunilk2	0.6204	0.6203	0.7926	0.7926	0.4031	0.4031	0.3483	0.348

    #return metapy.index.OkapiBM25(k1=0.9, b=0.75, k3=1.0)
    #sunilk2	0.6209	0.6168	0.7926	0.7868	0.4031	0.3966	0.35	0.3504

    #return metapy.index.OkapiBM25(k1=0.9, b=0.75, k3=1.2)
    #sunilk2	0.621	0.6203	0.7926	0.7926	0.4031	0.4031	0.3504	0.3482

    #return metapy.index.OkapiBM25(k1=1.0, b=0.75, k3=1.2)
    #sunilk2	0.6213	0.621	0.7924	0.7926	0.4009	0.4031	0.3527	0.3504



    #return metapy.index.OkapiBM25(k1=1.0, b=0.1, k3=1.2)
    #sunilk2	0.3734	0.621	0.3202	0.3518	0.3919	0.7924	0.4224	0.4009

    #return metapy.index.OkapiBM25(k1=1.0, b=0.8, k3=1.2)
    #sunilk2	0.6183	0.3734	0.352	0.3202	0.7887	0.3919	0.3946	0.4224

    #return metapy.index.OkapiBM25(k1=1.0, b=0.75, k3=1.2)
    #sunilk2	0.6213	0.6176	0.3527	0.3529	0.7924	0.7868	0.4009	0.3966

    #return metapy.index.OkapiBM25(k1=1.0, b=0.74, k3=1.2)
    #sunilk2	0.6215	0.6173	0.3513	0.3517	0.7935	0.7855	0.3996	0.4044

    #return metapy.index.OkapiBM25(k1=1.0, b=0.741, k3=1.2)
    #sunilk2	0.6216	0.6214	0.3516	0.3527	0.7935	0.7925	0.3996	0.4009
    #50th so far: Oct 22


    #return metapy.index.OkapiBM25(k1=1.0, b=0.742, k3=1.2)
    #sunilk2	0.6208	0.6216	0.3516	0.3516	0.7922	0.7935	0.3996	0.3996

    #return metapy.index.OkapiBM25(k1=1.0, b=0.699, k3=1.2)
    #sunilk2	0.6185	0.6208	0.3511	0.3516	0.7879	0.7922	0.4044	0.3996

    #return metapy.index.OkapiBM25(k1=1.0, b=0.742, k3=1.2)
    #sunilk2	0.6208	0.6216	0.3516	0.3516	0.7922	0.7935	0.3996	0.3996

    #return metapy.index.OkapiBM25(k1=1.0, b=0.749, k3=1.2)
    #sunilk2	0.6214	0.6188	0.3527	0.3514	0.7925	0.7889	0.4009	0.4004

    #return metapy.index.OkapiBM25(k1=1.0, b=0.73, k3=1.2)
    #sunilk2	0.6188	0.6215	0.3514	0.3513	0.7889	0.7935	0.4004	0.3996

    #return metapy.index.OkapiBM25(k1=1.0, b=0.7, k3=1.2)
    #sunilk2	0.6173	0.6183	0.3517	0.352	0.7855	0.7887	0.4044	0.3946

    #return metapy.index.OkapiBM25(k1=1.0, b=0.75, k3=1.0)
    #sunilk2	0.621	0.6211	0.3518	0.3521	0.7924	0.7924	0.4009	0.4009

    #return metapy.index.OkapiBM25(k1=0.8, b=0.75, k3=1.2)
    #sunilk2	0.62	0.6176	0.3463	0.3529	0.7931	0.7868	0.4024	0.3966

    #return metapy.index.OkapiBM25(k1=1.2, b=0.75, k3=1.2)
    #sunilk2	0.6176	0.6213	0.3529	0.3527	0.7868	0.7924	0.3966	0.4009

    #return metapy.index.OkapiBM25(k1=1.0, b=0.75, k3=0.5)
    #sunilk2	0.6208	0.62	0.3509	0.3463	0.7924	0.7931	0.4009	0.4024

    #return metapy.index.OkapiBM25(k1=1.0, b=0.75, k3=1.5)
    #sunilk2	0.6211	0.6208	0.3521	0.3509	0.7924	0.7924	0.4009	0.4009

    #return metapy.index.OkapiBM25(k1=0.9, b=1.0, k3=1.2)

    #return metapy.index.OkapiBM25(k1=0.9, b=0.75, k3=0)
    #sunilk2	0.6203	0.621	0.7926	0.7926	0.4031	0.4031	0.3482	0.3504

    #return metapy.index.OkapiBM25(k1=0.9, b=0, k3=1.2)
    #sunilk2	0.2587	0.56	0.2049	0.6918	0.4232	0.4161	0.3115	0.3445

    #return metapy.index.OkapiBM25(k1=0.9, b=0.5, k3=1.2)
    #sunilk2	0.56	0.6207	0.6918	0.7926	0.4161	0.4031	0.3445	0.3494

    #return metapy.index.OkapiBM25(k1=0.9, b=0.75, k3=1.5)
    #sunilk2	0.621	0.621	0.7926	0.7926	0.4031	0.4031	0.3504	0.3504

    #return metapy.index.OkapiBM25(k1=0.9, b=0.75, k3=2.0)
    #sunilk2	0.6207	0.6172	0.7926	0.7868	0.4031	0.3966	0.3494	0.3515

    #return metapy.index.OkapiBM25(k1=1.2, b=0.75, k3=1.5)
    #sunilk2	0.6172	0.621	0.7868	0.7926	0.3966	0.4031	0.3515

    #return metapy.index.OkapiBM25(k1=1.2, b=0.75, k3=7.0)
    #sunilk2	0.6168	0.6204	0.7868	0.7926	0.3966	0.4031	0.3504	0.3483

    #return metapy.index.OkapiBM25(k1=0.95, b=0.75, k3=800)
    #sunilk2	0.1496	-inf	0.0084	-inf	0.3477	0.3477	0.4027	0.4027

    #return metapy.index.OkapiBM25(k1=0.9, b=0.75, k3=100)
    #sunilk2	0.6203	0.6175	0.348	0.3465	0.7926	0.7895	0.4031	0.3983

    #return metapy.index.OkapiBM25(k1=0.9, b=0.6, k3=500)
    #sunilk2	0.589	0.5711	0.3444	0.3468	0.7406	0.7138	0.4129	0.3879

    #return metapy.index.OkapiBM25(k1=0.9, b=0.9, k3=800)
    #sunilk2	0.5711	0.6203	0.3468	0.348	0.7138	0.7926	0.3879	0.4031

    #return metapy.index.OkapiBM25(k1=0.9, b=0.8, k3=500)
    #sunilk2	0.6175	0.6203	0.3465	0.348	0.7895	0.7926	0.3983	0.4031

    #return metapy.index.OkapiBM25(k1=0.5, b=0.75, k3=500)
    #sunilk2	0.6177	0.62	0.3342	0.3483	0.7961	0.7924	0.397	0.4009

    #return mptf2ln(0.4, 500, 0.6, 0.7)
    #sunilk2	0.5803	0.5565	0.3463	0.352	0.73	0.6832	0.3845	0.4102

    #return PL2Ranker(0.5)
    #sunilk2	0.4658	0.5803	0.3506	0.3463	0.5366	0.73	0.386	0.3845

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: {} config.toml".format(sys.argv[0]))
        sys.exit(1)

    cfg = sys.argv[1]
    print('Building or loading index...')
    idx = metapy.index.make_inverted_index(cfg)
    ranker = load_ranker(cfg)
    ev = metapy.index.IREval(cfg)

    with open(cfg, 'r') as fin:
        cfg_d = pytoml.load(fin)

    query_cfg = cfg_d['query-runner']
    if query_cfg is None:
        print("query-runner table needed in {}".format(cfg))
        sys.exit(1)

    start_time = time.time()
    top_k = 10
    query_path = query_cfg.get('query-path', 'queries.txt')
    query_start = query_cfg.get('query-id-start', 0)

    query = metapy.index.Document()
    ndcg = 0.0
    num_queries = 0

    print('Running queries')
    with open(query_path) as query_file:
        for query_num, line in enumerate(query_file):
            query.content(line.strip())
            results = ranker.score(idx, query, top_k)
            ndcg += ev.ndcg(results, query_start + query_num, top_k)
            num_queries+=1
    ndcg= ndcg / num_queries
            
    print("NDCG@{}: {}".format(top_k, ndcg))
    print("Elapsed: {} seconds".format(round(time.time() - start_time, 4)))
